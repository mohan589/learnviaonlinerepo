# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary server in each group
# is considered to be the first unless any hosts have the primary
# property set.  Don't declare `role :all`, it's a meta role.

role :app, %w{173.214.173.101}
role :web, %w{173.214.173.101}
role :db,  %w{173.214.173.101}

set :deploy_to, '/home/deploy'

# set :nginx_server_name, %w{173.214.173.101}
# set :scm, :svn
# set :svn_user, 'mohan'
# set :svn_password, 'mohan143'

# set :repository,
  # Proc.new { "--username '#{svn_user}' " +
            #  "--password '#{svn_password}' " +
            #  "--no-auth-cache " +
              # "http://173.214.173.101:4000/svn/learnrepo/"
  # }




# Extended Server Syntax
# ======================
# This can be used to drop a more detailed server definition into the
# server list. The second argument is a, or duck-types, Hash and is
# used to set extended properties on the server.

server '173.214.173.101', user: 'root', password: '*123*mohan', roles: %w{web app db}, primary:true


# Custom SSH Options
# ==================
# You may pass any option but keep in mind that net/ssh understands a
# limited set of options, consult[net/ssh documentation](http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start).
#
# Global options
# --------------
#  set :ssh_options, {
#    keys: %w(/home/rlisowski/.ssh/id_rsa),
#    forward_agent: false,
#    auth_methods: %w(password)
#  }
#
set :ssh_options, {
	keys: %w(/home/mohan/.ssh/id_rsa),
	forward_agent: true,
	user: 'mohan',
#	verbose: :debug
	# auth_methods: %w(mohan143)
}

# And/or per server (overrides global)
# ------------------------------------
# server 'example.com',
#   user: 'user_name',
#   roles: %w{web app},
#   ssh_options: {
#     user: 'user_name', # overrides user setting above
#     keys: %w(/home/user_name/.ssh/id_rsa),
#     forward_agent: false,
#     auth_methods: %w(publickey password)
#     # password: 'please use keys'
#   }
