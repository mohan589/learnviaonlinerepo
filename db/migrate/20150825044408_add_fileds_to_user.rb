class AddFiledsToUser < ActiveRecord::Migration
  def change
    # add_column :users, :date_of_birth, :datetime
    # add_column :users, :mobile, :string
    # add_column :users, :first_name, :string
    # add_column :users, :last_name, :string
    # add_column :users, :user_name, :string
    # add_column :users, :current_password, :string
    add_column :users, :activation_digest, :string
    add_column :users, :activated, :string
    add_column :users, :activated_at, :string
    add_column :users, :image, :string
  end
end
