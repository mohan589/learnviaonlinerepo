class AccountActivationsController < ApplicationController
	    before_filter :require_no_user, :only => [:new, :create]

	def new
      @user = User.find_using_perishable_token(params[:activation_code], 1.week) || (raise Exception)
      raise Exception if @user.active?
    end

    def show
      @user = User.find(params[:id])

      # raise Exception if @user.activete?
			# binding.pry
      if @user.activated!
        # @user.deliver_activation_confirmation!
        # redirect_to account_url
				redirect_to products_path
      else
        render :action => :new
      end
    end

	def edit
	end

  def activation_done
  end

  def index
  end

end
